const express = require("express");


const app = express();


app.use(express.json());

app.use("/api",require("./routes/api.routes"));

app.listen(5000,() => {
    console.log("app started");
})