const db = require("../config/db");


const postSalary = (req,res) => {
    
    let data = [
        req.body.emp_id,
        req.body.month,
        req.body.year,
        req.body.amount,
        req.body.generated_date || new Date(),
    ]

    let sql = "INSERT INTO salary (emp_id,month,year,amount,generated_date) VALUES (?)";
    
    db.query(sql,[data],(err,result) => {
        if (err){
            console.log(err);
            return res.status(400).json({err:"internal server error"});
        };
        res.status(200).json({id:result.insertId});
    })

};

const putSalary = (req,res) => {
    
    let { id } = req.params;

    let sql = "UPDATE salary SET ? WHERE id = ?";
    
    db.query(sql,[req.body,id],(err,result) => {
        if (err){
            console.log(err);
            return res.status(400).json({err:"internal server error"});
        };
        
        res.status(200).json({id});
    })

};

const deleteSalary = (req,res) => {
    
    let { id } = req.params;

    let sql = "DELETE FROM salary WHERE id = ?";
    
    db.query(sql,[id],(err,result) => {
        if (err){
            console.log(err);
            return res.status(400).json({err:"internal server error"});
        };
        
        res.status(200).json({id});
    })

};

module.exports = {
    postSalary,
    putSalary,
    deleteSalary
}