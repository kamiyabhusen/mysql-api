const db = require("../config/db");


const postEmployee = (req,res) => {
    
    let data = [
        req.body.name,
        req.body.empno,
        req.body.deptid,
        req.body.joindate || new Date(),
        req.body.enddate || null
    ]

    let sql = "INSERT INTO employee (name,emp_no,dept_id,join_date,end_date) VALUES (?)";
    
    db.query(sql,[data],(err,result) => {
        if (err){
            console.log(err);
            return res.status(400).json({err:"internal server error"});
        };
        res.status(200).json({id:result.insertId});
    })

};

const putEmployee = (req,res) => {
    
    let { id } = req.params;

    let sql = "UPDATE employee SET ? WHERE id = ?";
    
    db.query(sql,[req.body,id],(err,result) => {
        if (err){
            console.log(err);
            return res.status(400).json({err:"internal server error"});
        };
        
        res.status(200).json({id});
    })

};

const deleteEmployee = (req,res) => {
    
    let { id } = req.params;

    let sql = "DELETE FROM employee WHERE id = ?";
    
    db.query(sql,[id],(err,result) => {
        if (err){
            console.log(err);
            return res.status(400).json({err:"internal server error"});
        };
        
        res.status(200).json({id});
    })

};

module.exports = {
    postEmployee,
    putEmployee,
    deleteEmployee
}