const db = require("../config/db");


const postDepartment = (req,res) => {
    
    let data = [
        req.body.name,
        new Date()
    ]

    let sql = "INSERT INTO department (name,created_date) VALUES (?)";
    
    db.query(sql,[data],(err,result) => {
        if (err){
            console.log(err);
            return res.status(400).json({err:"internal server error"});
        };
        res.status(200).json({id:result.insertId});
    })

};

const putDepartment = (req,res) => {
    
    let { id } = req.params;
    let { name } = req.body;

    let sql = "UPDATE department SET name = ? WHERE id = ?";
    
    db.query(sql,[name,id],(err,result) => {
        if (err){
            console.log(err);
            return res.status(400).json({err:"internal server error"});
        };
        
        res.status(200).json({id});
    })

};

const deleteDepartment = (req,res) => {
    
    let { id } = req.params;

    let sql = "DELETE FROM department WHERE id = ?";
    
    db.query(sql,[id],(err,result) => {
        if (err){
            console.log(err);
            return res.status(400).json({err:"internal server error"});
        };
        
        res.status(200).json({id});
    })

};

module.exports = {
    postDepartment,
    putDepartment,
    deleteDepartment
}