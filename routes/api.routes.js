const express = require("express");
const router = express.Router();

//controllers
const departmentController = require("../controllers/department.controller");
const employeeController = require("../controllers/employees.controller");
const salaryController = require("../controllers/salary.controller");


//department routes
router.post("/departments",departmentController.postDepartment);
router.put("/departments/:id",departmentController.putDepartment);
router.delete("/departments/:id",departmentController.deleteDepartment);

//Employee routers
router.post("/employees",employeeController.postEmployee);
router.put("/employees/:id",employeeController.putEmployee);
router.delete("/employees/:id",employeeController.deleteEmployee);

//Employee routers
router.post("/salaries",salaryController.postSalary);
router.put("/salaries/:id",salaryController.putSalary);
router.delete("/salaries/:id",salaryController.deleteSalary);

module.exports = router;